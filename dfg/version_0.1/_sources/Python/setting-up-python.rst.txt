==============================
EK's Python MacOSX Setup Guide
==============================

.. |date| date::

:Date: |date|
:Version: 0.1
:Reading Time: 15m
:Actual Doing Time: 30-60m


This is a highly opinionated guide to setting up python3 on MacOS X. This is written for Proof developers but it probably applies more generally. If you are a proof dev, you should probably follow this guide unless there is a good reason to #yolo.

There's a lot of conflicting information on the interwebs. After sifting through much of the literature online and much #headdesk, here's what I have settled on as the simplest means to get a clean, working python3 on MacOS 10.15. Bad Python installations cause all kinds of problems and gets in the way of happy, stress-free Python development.

My goal here is not to give you the most optimal Python setup; it is to give you the most robust and dependable initial foundation for Python development. Also, I hope to arm those starting at the Python dev trailhead with a basic understanding what's going on under the hood so that your pythonic journey doesn't begin with a purely algorithmic command of the fundamentals. Getting it right from the get-go will feel like starting the day with clean underwear.

QuickStart
----------

If you are new to python dev on MacOSX and you have not attempted to install python previously you can just skip most of this and go directly to the python.org_. Download and install the official releases of python 3.7 and python 3.8 of MacOSX. Make sure you read the `README` that comes with the releases.

Not-so-QuickStart
-----------------

If, however, you have installed `python2` or `python3` previously and find yourself a bit confused about where everything is, what follows is a guide to restoring your python to a clean, working state.

What do we want to achieve
==========================
When all this is done, we want to be able to:

* open up a terminal session and type ``python3`` and have absolutely confidence that it is running the latest stable version of Python (3.8 or whatever comes next).
* launch Python virtual environments with a known, good installation of an earlier stable release of Python such as Python 3.7.4
* be able to use ``pip`` without any fuss
* have the means to adopt a reliable, basic standard workflow for general Python development.

How do we get there (TL;DR)
===========================

1. Strip it back to basics
  #. Uninstall all global packages installed by pip3
  #. Uninstall all previous versions from homebrew
  #. Unlink all symlinks to Python and python3
2. Do a clean install
  #. Install a clean binary of Python 3.7 from python.org
  #. Install a clean binary of Python 3.8 from python.org
  #. Set your shell environment PATH var

Which Python
============

System Python (python 2.7)
***************************

In Mac OS 10.14 Catalina, the default Python that came with OS X was Python 2.7 which macOS puts in ``/System/Library/Frameworks/python.framework/Versions/2.7/bin/`` which is symlinked to ``/usr/bin/python``. Until Catalina I believe Python 2.7 was *still* the default version Python that shipped with Mac OS because it was system level required dependency. Sadly, running ``python`` in your terminal will result in your shell launching Python 2.7. If this truly bothers you, you can alias ``python`` to ``python3`` *after* python3 has been properly installed. I prefer to be explicit about launching ``python3``. For now, we are going to be leaving your system python2 alone. Do not attempt to delete or repoint python to python3 in ``/usr/bin`` lest we break things.

System Python3
**************

To make things even more confusing, in addition to Python 2.7, systems with MacOS X 10.15 Catalina and above also come with a stub for a system python3 binary pre-installed. That's right--TWO system pythons! If or when you install the Xcode command line developer tools, this stub becomes a fully constituted Python 3.7.3 under the ``/System/Library/Frameworks/python.framework/Versions/`` directory.

We don't care about system installed Pythons. And if you haven't installed a system python3 already, don't bother. If you have, leave it alone. We are gong to be installing an entirely separate instance of python3 using the official python.org_ installer for our day-to-day development use. Let your Mac have it's own python executables it needs.

Getting to Baseliine
====================

Now, if your computer is anything like mine, it is also probably littered with previous attempted or aborted installations of python3. Perhaps, you migrated from an earlier version of OS X to 10.15 and you have different versions python3 already installed. Perhaps, you started your Python walkabout without using virtual environments and you have all kinds of modules installed globally. Regardless, chances are that you have arrived at this point holding a confusing bag of mixed-up python3 versions, installations and packages from either `homebrew` or from the official Python site, or both.

We are going to be cleaning all this up--resetting your Mac back to its base system python executables and setting up a new, separate squeaky clean python3 environment, starting with unwinding Homebrew.

To Brew or Not To Brew 
**********************

Homebrew is great for experienced developers who use MacOSX. For a while, this was my go-to means of installing Python. Homebrew compiles software from its source or installs precompiled binaries if they are available onto your computer. The practice of using a package management system to maintain and update all your software is common practice in the opensource, Unix/Linux world; and because OS X is built on BSD which in turn is based on a Unix kernel there are lots of useful packages that can be installed this way. Think of Homebrew and package managers of its ilk like the Unix/Linux equivalent of Apple's Software Updater. In theory, by using Homebrew, it is easier to keep all the Unix-based software that you have installed or built by homebrew--including python--up to date. Also, I can't tell if I am just imagining this but I believe there was a time before Python 3.7 when the official Python binaries didn't seem to install nicely. All this is to say that it doesn't surprise me that much of the literature today continue to suggest using Homebrew for Python. 

Unfortunately, there be gotchas. Going the route of Homebrew isn't  exactly friction-free. For one, you will need to make sure Xcode command line tools and gcc (Gnu C Compiler) are first properly installed on your machine. Furthermore, brewing Python 3.x yourself doesn't always gaurantee that it'll build properly. And, critically, using Homebrew won't leave you with less of a mess if your Python installations are already a tangled jumble of conflicting old installations.

If you are comfortable with the concepts of installing software with Homebrew and if you are confident about troubleshooting your Python installations, by all means, continue to brew. This guide is probably not for you. However, if ``brew install`` has more or less been a faith-based activity and invoked like a magic spell without a clear grasp of its inner mysteries, it probably makes more sense to start your Python journey with the official releases. 

Personally, I've switched from Homebrew to using official releases from python.org_. For Proof developers and affiliates, I am recommending that you wear masks in public and use the official Python releases at python.org_.

.. _python.org: https://python.org

About pip
*********
As part of this effort to get back to a clean baseline, we want to pare back our globally installed Python packages. Most third party packages should be installed in your Python project's virtual environment--some notably exceptions might be Jupyter, DocUtils or Sphinx. But, as a general best practice, don't use global modules for dev work. This makes it managing dependencies and working collaboratively easier and your global environment less brittle. 

In order to clean up all your globally installed Python modules, we need dive into Pip. 

Pip is the python module that is used to install third- party Python packages from the PyPI repository. Pip normally comes bundled with Python 2.7 > or Python 3.4 > installation as the official python.org_ releases, or as part of a Homebrew installation. It can be installed separately but you shouldn't do that because you'll just lose track of which Pip was installed, and where. Pip itself is specific to each major python version. And, the packages that have been installed globally by Pip are installed in the ``site-packages`` directory of the Python version running Pip. For example, if you have three versions of Python, 2.7, 3.7.4, 3.8.2, all three will have their own Pip and their own site-packages. If you have previously flipped between installing Homebrew and the official python bundles, you probably have Python packages scattered in different ``lib/pythonX.Y/site-packages`` locations.

Which Pip
*********

On MacOSX, by default, the ``pip`` shell command (without a trailing number) is linked to the system's pre-installed Python 2.7. Sometimes, because this hasn't always been consistently implemented on the Mac since Python 3's released, the``pip`` command can point to a Python 3's pip. The ``pip3`` command, on the other hand, (with the trailing number, '3') should always refer to the Pip that comes bundled with the version of Python linked to ``python3`` in your shell. 

Yes this is confusing. 

The main takeaway here is that every pip is bound to a python installation. ``pip3`` refers to the pip that is bundled with some python3 installation. Which pip gets called depends entirely on which python or python3 installation is first in your environment path. Therefore, it's not sufficient for us to uninstall packages with a naive ``pip3 --uninstall`` command. You've to go uninstall the site-packages that are stored with each separate installation of python. For the purposes of this guide, we are going to ignore any Python 2 site-packages. We are done with python 2.7 at Proof, and we don't need to look back.

Uninstalling Packages
*********************

Here's how I go about this. There are faster ways to do this but this tries not to get too cute. The basic idea is to find out where all your python3 installations are, go to each of those ``bin/`` directory and run a ``./python3 -m pip freeze | xargs ./python3 -m pip uninstall -y``. The ``which`` command gets us all known executables in your PATH; for a more exhaustive find, you can also look at using the find [#find]_ command.

Example:

::

  $ which -a python3
  /Library/Frameworks/Python.framework/Versions/3.8/bin/python3
  /Library/Frameworks/Python.framework/Versions/3.7/bin/python3
  /usr/local/bin/python3
  /usr/bin/python3
  cd /Library/Frameworks/Python.framework/Versions/3.8/bin/
  ./python3 -m pip freeze | xargs ./python3 -m pip uninstall -y

Do this for all the python3 installations you have. Once this is done, you should be able to do ``pip3 freeze`` and get nothing back.

Uninstall Homebrew's Python3
****************************

At this point, if you have Homebrew installations of python3, go ahead an uninstall them with ``brew uninstall python3``. Check your ``/usr/local/bin`` directory for any symlinked python3 files with  ``ls -la python3*``.

If you see any links to the Homebrew's installation directory such as ``/usr/local/lib/opt``, you should go ahead and unlink that ``python3`` with the unlink command.

::

  unlink python3

.. Warning::
  Make sure you only unlink any python3 files point to defunct Homebrew installations of python3

You may have to do a ``brew list python3`` to find all the versions of python3 you have installed via brew and uninstall each one. If you don't have Homebrew, skip to the next step.

Installing Python3
******************
Check to see if you have the official versions of python3.7 and python3.8 installed. Official python.org_ installations are found under ``/Library/Frameworks/Python.framework/Versions/3.7/bin/python3.x``. For example, on my computer, I see this:

::

  $ which -a python3.7
  /Library/Frameworks/Python.framework/Versions/3.7/bin/python3.7
  /usr/local/bin/python3.7

If you see ``/Library/Frameworks/Python.framework/Versions/3.7/``, it means Python 3.7 is happily installed. If not, go ahead and go to to python.org/downloads and install the latest release of Python 3.7, such as Python 3.7.7

Do the same for python3.8. On my Mac, I see this:
::

  $ which -a python3.8
  /Library/Frameworks/Python.framework/Versions/3.8/bin/python3.8
  /usr/local/bin/python3.8

The order of installation matters, here. You the to install the most current version of Python last. We need both Python 3.7 and Python 3.8 at Proof, but python3 should be pointed to the latest version, 3.8.2 in this instance. To find out what version of Python run the following command in terminal:

::

  $ python3 --version
  Python 3.8.2 

Next, check to make sure Python 3.7 is accessible with the ``python3.7`` command: 

::

  $ python3.7 --version
  Python 3.7.7

Next, :doc:`ek-python-workflow`

.. rubric:: Footnotes

.. [#find] to exhaustively find all instances of pip or anything else, you can use the ``find`` command. ``which`` justs tells us which executables are known to you via the ``PATH`` environment variable