===============
The DFG Process
===============

Tracks
------
The DFG consists of three tracks: a global process track, a standards track, and an informational track.

Process track
=============
Global process guide for developing DFG guides.

Standards track
===============
Officially adopted standards and conventions at Proof, delineated by subject area
  
Informational track
===================
How-tos, strategies, solutions by subject area

Subject Areas
-------------
.. list-table::
    :header-rows: 1

    * - Technology Subject Area
      - Code
    * - FileMaker 
      - FM
    * - Claris Connect 
      - CC
    * - Python
      - PY
    * - Django
      - DJ
    * - Solr
      - SL
    * - Javascript
      - JS
    * - Swift/SwiftUI
      - SW

Proposals
---------

There are two ways to submit DFG proposals and to contribute to this document. The default way is through the Bitbucket issues queue which you can find at `<https://bitbucket.org/proofgroup/proofgroup.bitbucket.io/>`_. Just submit a ticket there using the simple template below. If you have been granted access to the master JIRA DFG project, you are welcome to submit there, too.

Proposals that have been accepted will be registered and tracked (as a DFG project issue, with a registry number), DFG committee will produce drafts  for comments, make rounds of edits to incorporate changes, suggestions, and finally release each proposal as part of the official DFG.
