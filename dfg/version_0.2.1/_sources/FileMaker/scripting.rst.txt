====================
Scripting Guidelines
====================

Summary
-------

How to write a script is as much about the individual developer style as anything. Proof's 

Script Types
------------

- API
- Model
- User Interaction
- Event Triggers

General Guidelines
------------------

- Script Structure
- Parameters
- Comments
- Introspection
- Error Guard Blocks
- Cleanup
- Return Structure

Cleanup
-------

After the script has completed operations but before we exit we determine whether or not the operation
was successful. Part of the error control process is executing a set of cleanup steps to ensure the
database state is as stable as possible before releasing control. We also want to minimize any data
leakage which could present a security vulnerability. This "cleanup" process minimally involves restoring 
the  starting context of the script, clearing any global fields used, and ensuring any record locks have 
been released. 

API Scripts
-----------

Model Scripts
-------------

User Interaction
----------------

Event Triggers
--------------


