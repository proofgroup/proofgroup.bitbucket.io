Recommended Tools & Utilities
=============================

IDE
  *VSCode* -- This is my recommended development IDE for most general developers at Proof. It works well on Mac, Windows and Linux. Officially, I love and endorse PyCharm for any hardcore python work, but *VSCode* is should be fine for most of us. If you have a strong preference for something else such as vim, emacs, Textmate, or whatever, that's fine, just know that neither I nor Andy will be much help.

shell
  ``zsh`` -- This is now default on OS 10.15 Catalina

Python dependency/package manager
  ``pip3``

Python virtual environment module
  the built in ``venv`` module that comes with python > 3.7. See, https://docs.python.org/3/library/venv.html

Terminal
  ``iTerm2`` -- The standard Terminal app that comes with MacOS X is just fine for most things. Upgrade to iTerm2 later if you want additional bell and whistles.