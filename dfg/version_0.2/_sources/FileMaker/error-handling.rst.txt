===============================
Error Handling in FileMaker Pro
===============================

Summary
-------
Exception handling has long been ignored in FileMaker applications. Here we explain why exception handling is important, and describe both the process-control mechanisms and the data structures used to handle exceptions within a FileMaker script.


The Why of Exception Handling
=============================

  *My first program taught me a lot about the errors that I was going to be making in the 
  future, and also about how to find errors. That's sort of the story of my life, making 
  errors and trying to recover from them. I try to get things correct. I probably obsess 
  about not making too many mistakes.* - Donald Knuth

Like all programming, FileMaker applications are not immune to exceptions, whether those arise
from the user interacting with the native program itself or via the design and scripting the 
developer has put in place. As a database application, exceptions can have catastrophic impact 
on not just the operation of the program, but also the integrity of the data. For many developers
error handling is an afterthought, if it is a thought at all. Some developers see the error
messages FileMaker produces as a nuisance, turning **Error Capture** on without going through the
trouble of actually checking for errors.

Nearly every step in a FileMaker script can fail, for any number of reasons. Consider the following
script section:

::

    Go To Layout
    Show All Records
    Delete All Records/Requests [ No Dialog ]

Each of these steps has a possibility of failure. A developer only checking for failure of the third 
line misses the possibility that either of the first two lines might have failed, causing either 
unexpected or possibly catastrophic results.

Proof practice is to check for the possibility of failure of every operation, and to handle such 
failure appropriately. It does so in three distinct areas: environment validation, process control,
and returning process results to calling scripts.

In addition to exceptions raised by the FileMaker application – which are caught using the 
``Get ( LastError )`` function – the developer may wish to raise their own exceptions. We encourage 
this practice using the same methods and mechanics of application exceptions. The point of raising 
and catching exceptions is to avoid making any assumptions about the success or failure of a 
particular operation. 

To make exception handling accessible to the developer, it shouldn't require anything outside of 
the native toolset to implement. While plug-ins and custom functions help encapsulate some tasks, 
requiring their use means the developer will get around to handling errors later. (*hint: later 
rarely comes*) To the extent that a developer chooses to use a custom function or plug-in function,
these guidelines advise against making such use mandatory.

The How of Exception Handling
=============================

Error Data structure
--------------------

For consistency between scripts we advocate the use of the following data structures for recording
errors during the operation of the script.

**$lastError**

The error code is a numeric value indicating whether or not an error has occurred. A zero value 
indicated no error was encountered, while a non-zero value denotes an exception. FileMaker
exceptions are raised by setting an internal flag *LastError* to a code indicating which specific
exception was encountered. Our variable is named to mimic this flag and the values we use are
either directly set from the internal flag or correspond to existing values. Any custom error
codes are indicated with a negative number.

**$errorMessage**

The error message is a human-readable explanation of the context in which the exception occurred.
This message may be displayed to the user to indicate an exception occurred and therefore should be
written with that purpose in mind.

**$errorDescription**

The error description includes details the developer can use to debug the application. There is no 
prescribed format for this description, however it is good practice to include the environment along
with a descriptoin of the error that occurred.

Environment Validation
----------------------

In general scripts should not assume their environment. If not setting environment explicitly, they
should minimally test that they are operating within the correct environment. This includes validating
the following:

- Script Parameters
   Have all required parameters been supplied to the script? Are they in the correct format? Do
   referenced objects exist?
- Window Mode
   Are we operating in the correct mode? Was an exception raised on attempting to switch modes?
- Layout/Context
   Are we on the correct layout? Is our script operating on the correct table context?

When the environment is invalid the **$lastError** variable should be set to force
script execution to fall through to the end.

Process Control
---------------

Different exceptions mean our script should not continue with execution and should instead exit. One
long-standing convention is that a procedure should only exit in one place.

Directives
~~~~~~~~~~

FileMaker has no real "Try/Catch" analogue. Depending on the environment (e.g. whether being executed
on an interactive client or with Server scripting) a script will continue to execute despite any errors
that have occurred. There are two directives which control the behavior of a script when it encounters
an error.

::

  Set Error Capture []
  Allow User Abort []

When enabled, the **Set Error Capture** directive will prevent most exceptions from displaying an error 
dialog to the end user. This allows the developer to determine how to handle the exception.

In the context of an exception, the **Allow User Abort** directive will determine whether or not the user
is able to cancel the script process if an error dialog is displayed. For scripts run without user 
interaction (e.g. Scheduled Server Scripts) the **Allow User Abort** directive determines whether or not
script execution will continue or be aborted.

Fall-through Process
~~~~~~~~~~~~~~~~~~~~

The principle pattern used is to enclose each operation or group of operations within a guarded block.
These blocks allow execution of the script to "fall through" to the end, where any global (to the script) 
exception handling occurs. In this manner the script has a single, predicatble point of exit during 
operation, alllowing the developer to specify any cleanup operations a single time.

::

  If [ not $lastError ]
    <operation>
    Set Variable [ $lastError Value: Get ( LastError )]
  End If

Within each guarded block the error code for the exception (if any) resulting from the operation is copied 
from the internal flag into the corresponding **$lastError** script variable. The exception can then be handled
in one of several ways: directly within the assignment step, within the block, or deferred until the end of 
the script.

Handling Exceptions (or not)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Just as important as catching the exception is handling it once it's been caught. One of the benefits of
the fall-through guard block design is that if followed at all it prevents exceptions from causing
additional harm to the database. For example, if the ``Go To Layout`` step fails, the subsequent ``Show
All Records`` and ``Delete All Records/Requests`` steps will be skipped.

::
  
  If [ not $lastError ]
    Go To Layout [ PEOPLE cache table ( PEOPLEcache ) ]
    Set Variable [ $lastError ; Value: Get ( LastError ) ]
  End If
  If [ not $lastError ]
    Show All Records
    Set Variable [ $lastError ; Value: Get ( LastError ) ]
  End If
  If [ not $lastError ]
    Delete All Records [ With dialog: Off ]
    Set Variable [ $lastError ; Value: Get ( LastError ) ]
  End If

This pattern attempts to ensure that exceptions "do no (additional) harm." To provide more of a benefit
the pattern can be extended to handle and potentially recover from exceptions. To start, not all
exceptions are considered fatal. The script excerpt above is part of a larger script that dumps and
reloads a record cache. If the cache is empty FileMaker will throw an exception (error code 3) when we 
attempt to ``Delete All Records``. Knowing this, we can choose to ignore that exception by handling it
inline, allowing the script to continue execution if we encounter it.

::

  If [ not $lastError ]
    Delete All Records [ With dialog: Off ]
    Set Variable [ $lastError ; Value: Case ( Get ( LastError ) = 3 ; 0 ; Get ( LastError ) ]
  End If

An alternative is to nest exception handling code inside of the guard block.

::

  If [ not $lastError ]
    Set Field [ locator::addressID ; $addressID ]
    Set Variable [ $lastError ; Value: Let ( __lastError = Get ( LastError ); Case ( not __lastError
    and IsEmpty ( locator ADDRESS::ID ); 401; __lastError ) ]
    If [ $lastError = 401 ]
        Set Variable [ $parameter ; Value: null ]
        Perform Script [ Specified: From list ; "new Address ( addressObject )" ; Parameter: $parameter ]
        Set Variable [ $lastError ; Value: Let ( [ __lastError = Get ( $lastError ) ; __scriptResult = 
            Get ( ScriptResult ) ; __scriptError = JSONGetElement ( __scriptResult ; 
            "errorMessage".O."errorCode" ) ] ; Case ( not __lastError ; __scriptError ; __lastError ) ]
    End If
  End If

For critical errors, sometimes the preferred action is to exit the script and return control to the calling
process. Rather than issuing an ``Exit Script`` command the preferred method is to set the **$lastError**
variable to a non-zero numeric value. This will ensure that execution "falls through" to the end of the
script where the exception can be handled globally - generally through logging and setting the error
structure to be returned to the calling script.


Script Results
--------------

That a script has completed is no indication as to whether or not it has completed successfully. During
operation any number of exceptions might have occurred, causing the script to return without having
worked as intended. Ideally a script will return an error indicating that it was not able to complete
successfully. Here, whether or not the script has returned an error, the adage of "trust but verify" is
useful for the calling script. When calling another script we can read the error value returned by the
script, but if we are also expecting additional data be returned it is prudent to verify that we have
received the data we were expecting.

Every script should return a value indicating whether or not it was successful, and ideally if not 
successful what the critical exception was. The calling script will then use this value to determine
how to handle the exception (in this case the returning script is considered to have raised the 
exception rather than the database engine).

The FileMaker Data API (DAPI) returns an exception list as a JSON array called "messages" and for 
consistency in parsing errors we have adopted that return syntax. The intention is to not have differing
syntax for parsing return results when calling Proof scripts or DAPI scripts - the developer will be
able to use the same syntax when parsing the results from either.

The initial element in the array ("messages".0) is used to denote whether the script operation was 
successful overall. Additional array elements are optional, and are used to indicate whether other,
generally non-fatal exceptions were caught.

.. _messages:
::

  {
    "messages" : [
      { "code" : "0", "message" : "No error" },
      { "code" : "401", "message" : "Address not found - creating." }
    ]
  }    


Communicating to the User
~~~~~~~~~~~~~~~~~~~~~~~~~

For scripts that interact with the user errors should generally be communicated to inform the user as
to the nature of the exception and to optionally provide the user with the opportunity to cure the error 
before proceding.

A specific error variable, **$errorMessage**, is used to inform the operator of the exception using plain 
language as to the nature of the exception, and what steps they can take (if any) to address the problem.
The method chosen to inform the user is at the discretion of the developer, however there are patterns_ 
that are useful to provide a consistent experience to the user for various feedback scenarios.

.. _patterns: https://proofgroup.bitbucket.io/dfg/FileMaker/development_patterns.html

In general, only scripts operating at the UI layer should provide direct feedback to the user in an
interactive manner. All model, method, and API scripts should use the return results to communicate any
exceptions that occurred during operation.


Logging (optional)
~~~~~~~~~~~~~~~~~~

During development and after deployment it is often helpful to have a way to detrmine what exceptions are
have been raised, both fatal and non. A default log is provided in the proof_AUDITLOG_ file or the
developer can supply their own logging mechanism.

.. _proof_AUDITLOG: fmp://triton.proofgroup.com/proof_AUDITLOG
